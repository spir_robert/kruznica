#include "Kruynica.h"




Kruynica::Kruynica(Bod A, int r)
{
	this->A = A;
	this->r = r;
}

Kruynica::~Kruynica()
{
}

void Kruynica::Vypis_Rovnicu()
{
	std::cout << "stredova rovnica kruznice: (x - " << this->A.Daj_X() << ")^2 + (y - " << this->A.Daj_Y() << ")^2 = " << this->r*this->r << std::endl;
}

bool Kruynica::Lezi_Bod(Bod B)
{
	if ((B.Daj_X() - A.Daj_X())*(B.Daj_X() - A.Daj_X()) + (B.Daj_Y() - A.Daj_Y())*(B.Daj_Y() - A.Daj_Y()) == r*r)
		return true;
	else return false;
}
