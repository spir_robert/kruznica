#pragma once
#include "Bod.h"
#include <iostream>
class Kruynica
{
public:
	Kruynica(Bod A, int r);
	~Kruynica();
	void Vypis_Rovnicu();
	bool Lezi_Bod(Bod B);

private:
	Bod A;
	int r;
};

