#include <array>
#include <iostream>
#include "Bod.h"
#include "Kruynica.h"
int main()
{
	std::array<int, 10> pole;
	for (size_t i = 0; i < pole.size(); i++)
	{
		pole[i] = i;
	}

	for (size_t i = 0; i < pole.size(); i++)
	{
		std::cout << pole[i] << std::endl;
	}

	std::array<std::array<int, 10>, 10> pole2d;

	for (size_t i = 0; i < pole2d.size(); i++)
	{
		for (size_t j = 0; j < pole2d[i].size(); j++)
		{
			pole2d[i][j] = i*j;
		}
	}

	Bod A(0, 0);
	Kruynica kruh(A, 1);
	kruh.Vypis_Rovnicu();
	if (kruh.Lezi_Bod(Bod(1, 1)))std::cout << "bod na kruznici lezi" << std::endl;
	else std::cout << "bod na kruznici nelezi" << std::endl;
	getchar();
	return 0;
}
